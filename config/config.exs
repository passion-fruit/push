# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :push,
  ecto_repos: [Push.Repo]

# Configures the endpoint
config :push, PushWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "qVS77dj5uFSdiJGdrzoBBp3rUKCwnx6DwASpz+vJuRKvS5dyjXM60lJo0paE8Voc",
  render_errors: [view: PushWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Push.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Sentry config for reporting exceptions
config :sentry, dsn: System.get_env["SENTRY_API_URL"] || "${SENTRY_API_URL}",
  included_environments: [:prod, :test],
  environment_name: Mix.env,
  enable_source_code_context: true,
  root_source_code_path: File.cwd!

config :ex_aws,
  access_key_id: [System.get_env["AWS_ACCESS_KEY_ID"] || "${AWS_ACCESS_KEY_ID}", :instance_role],
  secret_access_key: [System.get_env["AWS_SECRET_ACCESS_KEY"] || "${AWS_SECRET_ACCESS_KEY}", :instance_role],
  s3: [
    scheme: "https://",
    host: "pushhorse.s3.amazonaws.com",
    region: "eu-west-1"
  ]

config :push, PushWeb.Mailer,
  adapter: Bamboo.MailgunAdapter,
  api_key: System.get_env["MAILGUN_API_KEY"] || "${MAILGUN_API_KEY}",
  domain: System.get_env["MAILGUN_DOMAIN"] || "${MAILGUN_DOMAIN}"


# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
