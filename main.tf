# How does Terraform know what order to create resources in ?
# When one resource references another resource, you create an implicit dependency.
# Terraform parses these dependencies, builds a dependency graph from them, and uses that to automatically figure out in
# what order it should create resources (e.g. Terraform knows it needs to create the security group before using it with
# the EC2 Instance).

provider "aws" {
  region = "eu-west-1" // This is a region in AWS land.
}


resource "aws_codedeploy_app" "push_horse" {
  name = "push_horse"
}


data "aws_elb_service_account" "main" {}
data "aws_caller_identity" "current" {}

# Create S3 bucket for application
resource "aws_s3_bucket" "b" {
  bucket = "push-horse"
  force_destroy = true
}

# S3 bucket policy to allow LB access log storage.
resource "aws_s3_bucket_policy" "b" {
  bucket = "${aws_s3_bucket.b.id}"
  policy =<<EOF
{
  "Id": "Policy1529769313048",
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1529769300618",
      "Action": ["s3:PutObject"],
      "Effect": "Allow",
      "Resource": "arn:aws:s3:::push-horse/*",
      "Principal": { "AWS": ["${data.aws_caller_identity.current.account_id}", "${data.aws_elb_service_account.main.arn}"] }
    }
  ]
}
EOF
}


# Create a VPC
# Documentation https://www.terraform.io/docs/providers/aws/r/vpc.html
resource "aws_vpc" "push_horse-vpc" {
  cidr_block = "10.0.0.0/24"
  enable_dns_hostnames = true

  tags {
    Name = "push_horse-aws-vpc"
  }
}


# Create an internet gateway to give our subnet access to the outside world.
resource "aws_internet_gateway" "push_horse-internet-gateway" {
  vpc_id = "${aws_vpc.push_horse-vpc.id}"
}


# Grant the VPC internet access on its main route table
resource "aws_route" "internet_access" {
  route_table_id         = "${aws_vpc.push_horse-vpc.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.push_horse-internet-gateway.id}"
}


data "aws_availability_zones" "available" {}

# Create a subnet to launch our instances into.
resource "aws_subnet" "push_horse-public-subnet1" {
  vpc_id = "${aws_vpc.push_horse-vpc.id}"
  # availability_zone = "eu-west-1a"
  cidr_block = "10.0.0.0/25"
  map_public_ip_on_launch = true
  availability_zone = "${data.aws_availability_zones.available.names[0]}"
}

# Create a subnet to launch our instances into.
resource "aws_subnet" "push_horse-public-subnet2" {
  vpc_id = "${aws_vpc.push_horse-vpc.id}"
  # availability_zone = "eu-west-1a"
  cidr_block = "10.0.0.128/25"
  map_public_ip_on_launch = true
  availability_zone = "${data.aws_availability_zones.available.names[1]}"
}


# Create subnet group. Needed for RDS instance to be created.
resource "aws_db_subnet_group" "push_horse-subnet-group-db" {
  name = "push_horse-subnet-group-db"
  description = "Our main group of subnets"
  subnet_ids = ["${aws_subnet.push_horse-public-subnet1.id}", "${aws_subnet.push_horse-public-subnet2.id}"]
  tags {
      Name = "Push Horse DB subnet group"
  }
}


# Create a security group for the ELB so it is accessible via the web
resource "aws_security_group" "push_horse-security-group-elb" {
  name = "push_horse-security-group-elb"
  vpc_id = "${aws_vpc.push_horse-vpc.id}"

  # Inbound
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"

    # CIDR blocks are a concise way to specify IP address ranges.
    # For example, a CIDR block of 10.0.0.0/24 represents all IP addresses between 10.0.0.0 and 10.0.0.255.
    # The CIDR block 0.0.0.0/0 is an IP address range that includes all possible IP addresses, so the security group
    # above allows incoming requests on port 8080 from any IP.
    # https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Outbound
  # Since the ELB is performing health checks we need to allow outbound requests.
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


# Create a security group for the launch config so instances can accept incoming
# requests from the elb
resource "aws_security_group" "push_horse-security-group-asg-launch-config" {
  name = "push_horse-security-group-asg-launch-config"
  vpc_id = "${aws_vpc.push_horse-vpc.id}"

  # Inbound
  ingress {
    from_port = 4000
    to_port = 4000
    protocol = "tcp"
    security_groups = ["${aws_security_group.push_horse-security-group-elb.id}"]
  }

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    security_groups = ["${aws_security_group.push_horse-security-group-elb.id}"]
  }

  # Outbound
  # Since the ELB is performing health checks we need to allow outbound requests.
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


# Create a load balancer - ELB (Elastic Load Balancer)
# Note that, by default, ELBs don’t allow any incoming or outgoing traffic (just like EC2 Instances), so you need to add
# a security group to explicitly allow incoming requests.
# Note:
#   If you are using a VPC then when you create an ELB you need to specify a subnet. Otherwise you use availability
#   zones.
resource "aws_elb" "push_horse-web-elb" {
  name = "pushhorse"
  subnets = ["${aws_subnet.push_horse-public-subnet1.id}", "${aws_subnet.push_horse-public-subnet2.id}"]
  security_groups = ["${aws_security_group.push_horse-security-group-elb.id}"]
  connection_draining = true

  # To route requests you add one or more “listeners” which specify what port the ELB should listen on and what port it
  # should route the request to.
  # listener {
  #   lb_port = 80
  #   lb_protocol = "http"
  #   instance_port = 80
  #   instance_protocol = "http"
  # }

  listener {
    instance_port      = 4000
    instance_protocol  = "http"
    lb_port            = 443
    lb_protocol        = "https"
    ssl_certificate_id = "${aws_acm_certificate.cert.arn}"
  }

  # The ELB has one other nifty trick up it’s sleeve: it can periodically check the health of your EC2 Instances and, if
  #  an instance is unhealthy, it will automatically stop routing traffic to it.
  health_check {
    healthy_threshold = 3
    unhealthy_threshold = 3
    timeout = 6
    interval = 20
    target = "HTTP:80/"
  }


  access_logs {
    bucket  = "push-horse"
    bucket_prefix  = "production-lb-access-logs"
    enabled = true
    interval = 60
  }
}


# Output the ELB's DNS name.
output "elb_dns_name" {
  value = "${aws_elb.push_horse-web-elb.dns_name}"
}


resource "aws_acm_certificate" "cert" {
  domain_name = "*.pushhorse.com"
  validation_method = "EMAIL"
}

# I registered a domain via route 53.
data "aws_route53_zone" "primary-registrar" {
  name         = "pushhorse.com."
}

resource "aws_route53_record" "non-www" {
  zone_id = "${data.aws_route53_zone.primary-registrar.zone_id}"
  name    = "pushhorse.com"
  type    = "A"

  alias {
    name = "${aws_elb.push_horse-web-elb.dns_name}"
    zone_id = "${aws_elb.push_horse-web-elb.zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "www" {
  zone_id = "${data.aws_route53_zone.primary-registrar.zone_id}"
  name    = "www.pushhorse.com"
  type    = "A"

  alias {
    name = "${aws_elb.push_horse-web-elb.dns_name}"
    zone_id = "${aws_elb.push_horse-web-elb.zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_acm_certificate_validation" "cert" {
  certificate_arn = "${aws_acm_certificate.cert.arn}"
}

# https://docs.aws.amazon.com/codedeploy/latest/userguide/getting-started-create-iam-instance-profile.html
resource "aws_iam_instance_profile" "pushhorse-instance-profile" {
  name = "pushhorse-instance-profile"
  roles = ["${aws_iam_role.pushhorse-instance-profile-iam-role.name}"]
}


resource "aws_iam_role" "pushhorse-instance-profile-iam-role" {
  name = "pushhorse-instance-profile-iam-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "pushhorse-instance-profile-iam-role-policy" {
  name = "pushhorse-instance-profile-iam-role-policy"
  role = "${aws_iam_role.pushhorse-instance-profile-iam-role.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:Get*",
        "s3:List*"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "pushhorse-instance-profile-iam-role-policy-CloudWatchLogsFullAccess" {
    role = "${aws_iam_role.pushhorse-instance-profile-iam-role.id}"
    policy_arn = "arn:aws:iam::aws:policy/CloudWatchEventsFullAccess"
}



# Create a launch configuration for instances inside an ASG (Auto Scaling Group).
# Note: No security groups required since the ASG will be inside a VPC.
resource "aws_launch_configuration" "push_horse-launch-config" {
  image_id = "ami-add175d4"
  instance_type = "t2.micro"
  key_name = "karl"
  security_groups = ["${aws_security_group.push_horse-security-group-asg-launch-config.id}"]
  iam_instance_profile = "${aws_iam_instance_profile.pushhorse-instance-profile.id}"


  # Adding this string(EOF) to the user data means AWS will execute this while booting the instance.
  # The script just sets up a web server that always returns 'Hello, World.'
  user_data = <<-EOF
              #!/bin/bash
              sudo apt-get -y update
              sudo apt-get -y install nginx
              sudo service nginx start

              sudo apt-get -y install ruby
              sudo apt-get -y install wget
              cd /home/ubuntu
              wget https://aws-codedeploy-eu-west-1.s3.amazonaws.com/latest/install
              chmod +x ./install
              sudo ./install auto
              sudo service codedeploy-agent startls
              sudo service codedeploy-agent status

              DD_API_KEY=8f26ff349522593083ae035f623994c5 bash -c "$(curl -L https://raw.githubusercontent.com/DataDog/datadog-agent/master/cmd/agent/install_script.sh)"
              EOF

  # create_before_destroy tells Terraform to always create a replacement resource before destroying an original (e.g.
  # when replacing an EC2 Instance, always create the new Instance before deleting the old one).
  lifecycle {
    create_before_destroy = true
  }
}


# Create the ASG
# https://github.com/hashicorp/terraform/issues/532 <- THE REASON FOR depends_on
resource "aws_autoscaling_group" "push_horse-asg" {
  depends_on = ["aws_launch_configuration.push_horse-launch-config"]
  launch_configuration = "${aws_launch_configuration.push_horse-launch-config.id}"
  vpc_zone_identifier = ["${aws_subnet.push_horse-public-subnet1.id}", "${aws_subnet.push_horse-public-subnet2.id}"]
  health_check_type = "EC2"
  min_size = 2
  max_size = 10
  # How does the ELB know which EC2 Instances to send requests to?
  # With an ASG, instances will be launching and terminating dynamically all the time..
  # To solve this you use the load_balancers parameter of the aws_autoscaling_group resource to tell the ASG to register
  # each Instance in the ELB when that instance is booting.
  load_balancers  = ["${aws_elb.push_horse-web-elb.id}"]

  tag {
    key = "Name"
    value = "push_horse-asg"
    # propage_at_launch (Required) Enables propagation of the tag to Amazon EC2 instances launched via this ASG
    propagate_at_launch = true
  }

  lifecycle { create_before_destroy = true }
}

# https://docs.aws.amazon.com/codedeploy/latest/userguide/getting-started-create-service-role.html
resource "aws_iam_role" "push_horse-codedeploy-deployment-iam-role" {
  name = "push_horse-codedeploy-deployment-iam-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "codedeploy.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "push_horse-codedeploy-deployment-iam-role-policy" {
  name = "push_horse-codedeploy-deployment-iam-role-policy"
  role = "${aws_iam_role.push_horse-codedeploy-deployment-iam-role.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "autoscaling:*",
        "codedeploy:*",
        "ec2:DescribeInstances",
        "ec2:DescribeInstanceStatus",
        "tag:GetResources",
        "sns:Publish",
        "s3:*"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "codedeploy-deployment-iam-role-policy-AWSCodeDeployRole" {
    role = "${aws_iam_role.push_horse-codedeploy-deployment-iam-role.id}"
    policy_arn = "arn:aws:iam::aws:policy/service-role/AWSCodeDeployRole"
}


# Create code deploy deployment group so deployments can be made into the ASG.
resource "aws_codedeploy_deployment_group" "push_horse-deployment-group" {
  app_name              = "${aws_codedeploy_app.push_horse.name}"
  deployment_group_name = "push_horse-deployment-group"
  service_role_arn      = "${aws_iam_role.push_horse-codedeploy-deployment-iam-role.arn}"
  deployment_config_name  = "CodeDeployDefault.HalfAtATime"

  deployment_style {
    deployment_option = "WITH_TRAFFIC_CONTROL"
    deployment_type   = "IN_PLACE"
  }

  load_balancer_info {
    elb_info {
      name = "${aws_elb.push_horse-web-elb.name}"
    }
  }

  auto_rollback_configuration {
    enabled = true
    events  = ["DEPLOYMENT_FAILURE"]
  }
}


# RDS security group for db.
resource "aws_security_group" "push_horse_security-group-rds" {
  name = "push_horse_production"

  description = "RDS postgres servers"
  vpc_id = "${aws_vpc.push_horse-vpc.id}"

  # Only postgres in
  ingress {
    from_port = 5432
    to_port = 5432
    protocol = "tcp"
    security_groups = ["${aws_security_group.push_horse-security-group-asg-launch-config.id}"]
  }

  # Allow all outbound traffic.
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


# Create RDS instance.
resource "aws_db_instance" "push_horse_production" {
  allocated_storage        = 10 # gigabytes
  backup_retention_period  = 7 # days
  db_subnet_group_name   = "${aws_db_subnet_group.push_horse-subnet-group-db.name}"
  engine                   = "postgres"
  engine_version           = "9.6"
  identifier               = "pushhorseproduction"
  instance_class           = "db.t2.small"
  multi_az                 = false
  name                     = "push_horse_production"
  # parameter_group_name     = "mydbparamgroup1" # if you have tuned it
  username                 = "push_horse_production"
  password                 = "123123123"
  port                     = 5432
  publicly_accessible      = false
  storage_encrypted        = true # you should always do this
  storage_type             = "gp2" # ssd
  publicly_accessible = true

  vpc_security_group_ids   = ["${aws_security_group.push_horse_security-group-rds.id}"]
}


# Datadog
resource "aws_iam_policy" "pushhorse_dd_integration_policy" {
  name        = "DatadogAWSIntegrationPolicy"
  path        = "/"
  description = "DatadogAWSIntegrationPolicy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "autoscaling:Describe*",
        "cloudwatch:Get*",
        "cloudwatch:List*",
        "ec2:Describe*",
        "ec2:Get*",
        "ecs:Describe*",
        "elasticloadbalancing:Describe*",
        "iam:Get*",
        "iam:List*",
        "logs:Get*",
        "logs:Describe*",
        "logs:TestMetricFilter",
        "rds:Describe*",
        "rds:List*",
        "route53:List*",
        "s3:GetBucketTagging",
        "sns:List*",
        "sns:Publish",
        "sqs:GetQueueAttributes",
        "sqs:ListQueues",
        "sqs:ReceiveMessage"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role" "pushhorse_dd_integration_role" {
  name = "DatadogAWSIntegrationRole"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": {
    "Effect": "Allow",
    "Principal": { "AWS": "arn:aws:iam::464622532012:root" },
    "Action": "sts:AssumeRole",
    "Condition": { "StringEquals": { "sts:ExternalId": "f73cf2b16e7647ab84b1fd4957eda08d" } }
  }
}
EOF
}

resource "aws_iam_policy_attachment" "pushhorse_allow_dd_role" {
  name       = "Allow Datadog PolicyAccess via Role"
  roles      = ["${aws_iam_role.pushhorse_dd_integration_role.name}"]
  policy_arn = "${aws_iam_policy.pushhorse_dd_integration_policy.arn}"
}

output "DataDog AWS Account ID" {
  value = "${aws_iam_role.pushhorse_dd_integration_role.arn}"
}

output "DataDog AWS Role Name" {
  value = "${aws_iam_role.pushhorse_dd_integration_role.name}"
}