defmodule Push.GettingStartedsTest do
  use Push.DataCase

  alias Push.GettingStarteds

  describe "getting_starteds" do
    alias Push.GettingStarteds.GettingStarted

    @valid_attrs %{step: "some step"}
    @update_attrs %{step: "some updated step"}
    @invalid_attrs %{step: nil}

    def getting_started_fixture(attrs \\ %{}) do
      {:ok, getting_started} =
        attrs
        |> Enum.into(@valid_attrs)
        |> GettingStarteds.create_getting_started()

      getting_started
    end

    test "list_getting_starteds/0 returns all getting_starteds" do
      getting_started = getting_started_fixture()
      assert GettingStarteds.list_getting_starteds() == [getting_started]
    end

    test "get_getting_started!/1 returns the getting_started with given id" do
      getting_started = getting_started_fixture()
      assert GettingStarteds.get_getting_started!(getting_started.id) == getting_started
    end

    test "create_getting_started/1 with valid data creates a getting_started" do
      assert {:ok, %GettingStarted{} = getting_started} = GettingStarteds.create_getting_started(@valid_attrs)
      assert getting_started.step == "some step"
    end

    test "create_getting_started/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = GettingStarteds.create_getting_started(@invalid_attrs)
    end

    test "update_getting_started/2 with valid data updates the getting_started" do
      getting_started = getting_started_fixture()
      assert {:ok, getting_started} = GettingStarteds.update_getting_started(getting_started, @update_attrs)
      assert %GettingStarted{} = getting_started
      assert getting_started.step == "some updated step"
    end

    test "update_getting_started/2 with invalid data returns error changeset" do
      getting_started = getting_started_fixture()
      assert {:error, %Ecto.Changeset{}} = GettingStarteds.update_getting_started(getting_started, @invalid_attrs)
      assert getting_started == GettingStarteds.get_getting_started!(getting_started.id)
    end

    test "delete_getting_started/1 deletes the getting_started" do
      getting_started = getting_started_fixture()
      assert {:ok, %GettingStarted{}} = GettingStarteds.delete_getting_started(getting_started)
      assert_raise Ecto.NoResultsError, fn -> GettingStarteds.get_getting_started!(getting_started.id) end
    end

    test "change_getting_started/1 returns a getting_started changeset" do
      getting_started = getting_started_fixture()
      assert %Ecto.Changeset{} = GettingStarteds.change_getting_started(getting_started)
    end
  end
end
