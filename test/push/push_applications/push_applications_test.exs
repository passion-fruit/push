defmodule Push.PushApplicationsTest do
  use Push.DataCase

  alias Push.PushApplications

  describe "push_applications" do
    alias Push.PushApplications.PushApplication

    @valid_attrs %{live_api_key: "some live_api_key", live_secret_key: "some live_secret_key", name: "some name", test_api_key: "some test_api_key", test_secret_key: "some test_secret_key", viewing_test_data: true}
    @update_attrs %{live_api_key: "some updated live_api_key", live_secret_key: "some updated live_secret_key", name: "some updated name", test_api_key: "some updated test_api_key", test_secret_key: "some updated test_secret_key", viewing_test_data: false}
    @invalid_attrs %{live_api_key: nil, live_secret_key: nil, name: nil, test_api_key: nil, test_secret_key: nil, viewing_test_data: nil}

    def push_application_fixture(attrs \\ %{}) do
      {:ok, push_application} =
        attrs
        |> Enum.into(@valid_attrs)
        |> PushApplications.create_push_application()

      push_application
    end

    test "list_push_applications/0 returns all push_applications" do
      push_application = push_application_fixture()
      assert PushApplications.list_push_applications() == [push_application]
    end

    test "get_push_application!/1 returns the push_application with given id" do
      push_application = push_application_fixture()
      assert PushApplications.get_push_application!(push_application.id) == push_application
    end

    test "create_push_application/1 with valid data creates a push_application" do
      assert {:ok, %PushApplication{} = push_application} = PushApplications.create_push_application(@valid_attrs)
      assert push_application.live_api_key == "some live_api_key"
      assert push_application.live_secret_key == "some live_secret_key"
      assert push_application.name == "some name"
      assert push_application.test_api_key == "some test_api_key"
      assert push_application.test_secret_key == "some test_secret_key"
      assert push_application.viewing_test_data == true
    end

    test "create_push_application/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = PushApplications.create_push_application(@invalid_attrs)
    end

    test "update_push_application/2 with valid data updates the push_application" do
      push_application = push_application_fixture()
      assert {:ok, push_application} = PushApplications.update_push_application(push_application, @update_attrs)
      assert %PushApplication{} = push_application
      assert push_application.live_api_key == "some updated live_api_key"
      assert push_application.live_secret_key == "some updated live_secret_key"
      assert push_application.name == "some updated name"
      assert push_application.test_api_key == "some updated test_api_key"
      assert push_application.test_secret_key == "some updated test_secret_key"
      assert push_application.viewing_test_data == false
    end

    test "update_push_application/2 with invalid data returns error changeset" do
      push_application = push_application_fixture()
      assert {:error, %Ecto.Changeset{}} = PushApplications.update_push_application(push_application, @invalid_attrs)
      assert push_application == PushApplications.get_push_application!(push_application.id)
    end

    test "delete_push_application/1 deletes the push_application" do
      push_application = push_application_fixture()
      assert {:ok, %PushApplication{}} = PushApplications.delete_push_application(push_application)
      assert_raise Ecto.NoResultsError, fn -> PushApplications.get_push_application!(push_application.id) end
    end

    test "change_push_application/1 returns a push_application changeset" do
      push_application = push_application_fixture()
      assert %Ecto.Changeset{} = PushApplications.change_push_application(push_application)
    end
  end
end
