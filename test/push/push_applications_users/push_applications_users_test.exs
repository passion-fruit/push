defmodule Push.PushApplicationsUsersTest do
  use Push.DataCase

  alias Push.PushApplicationsUsers

  describe "push_applications_users" do
    alias Push.PushApplicationsUsers.PushApplicationUser

    @valid_attrs %{}
    @update_attrs %{}
    @invalid_attrs %{}

    def push_application_user_fixture(attrs \\ %{}) do
      {:ok, push_application_user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> PushApplicationsUsers.create_push_application_user()

      push_application_user
    end

    test "list_push_applications_users/0 returns all push_applications_users" do
      push_application_user = push_application_user_fixture()
      assert PushApplicationsUsers.list_push_applications_users() == [push_application_user]
    end

    test "get_push_application_user!/1 returns the push_application_user with given id" do
      push_application_user = push_application_user_fixture()
      assert PushApplicationsUsers.get_push_application_user!(push_application_user.id) == push_application_user
    end

    test "create_push_application_user/1 with valid data creates a push_application_user" do
      assert {:ok, %PushApplicationUser{} = push_application_user} = PushApplicationsUsers.create_push_application_user(@valid_attrs)
    end

    test "create_push_application_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = PushApplicationsUsers.create_push_application_user(@invalid_attrs)
    end

    test "update_push_application_user/2 with valid data updates the push_application_user" do
      push_application_user = push_application_user_fixture()
      assert {:ok, push_application_user} = PushApplicationsUsers.update_push_application_user(push_application_user, @update_attrs)
      assert %PushApplicationUser{} = push_application_user
    end

    test "update_push_application_user/2 with invalid data returns error changeset" do
      push_application_user = push_application_user_fixture()
      assert {:error, %Ecto.Changeset{}} = PushApplicationsUsers.update_push_application_user(push_application_user, @invalid_attrs)
      assert push_application_user == PushApplicationsUsers.get_push_application_user!(push_application_user.id)
    end

    test "delete_push_application_user/1 deletes the push_application_user" do
      push_application_user = push_application_user_fixture()
      assert {:ok, %PushApplicationUser{}} = PushApplicationsUsers.delete_push_application_user(push_application_user)
      assert_raise Ecto.NoResultsError, fn -> PushApplicationsUsers.get_push_application_user!(push_application_user.id) end
    end

    test "change_push_application_user/1 returns a push_application_user changeset" do
      push_application_user = push_application_user_fixture()
      assert %Ecto.Changeset{} = PushApplicationsUsers.change_push_application_user(push_application_user)
    end
  end
end
