defmodule Push.PlatformTypesTest do
  use Push.DataCase

  alias Push.PlatformTypes

  describe "platform_types" do
    alias Push.PlatformTypes.PlatformType

    @valid_attrs %{name: "some name", uniq_id: "some uniq_id"}
    @update_attrs %{name: "some updated name", uniq_id: "some updated uniq_id"}
    @invalid_attrs %{name: nil, uniq_id: nil}

    def platform_type_fixture(attrs \\ %{}) do
      {:ok, platform_type} =
        attrs
        |> Enum.into(@valid_attrs)
        |> PlatformTypes.create_platform_type()

      platform_type
    end

    test "list_platform_types/0 returns all platform_types" do
      platform_type = platform_type_fixture()
      assert PlatformTypes.list_platform_types() == [platform_type]
    end

    test "get_platform_type!/1 returns the platform_type with given id" do
      platform_type = platform_type_fixture()
      assert PlatformTypes.get_platform_type!(platform_type.id) == platform_type
    end

    test "create_platform_type/1 with valid data creates a platform_type" do
      assert {:ok, %PlatformType{} = platform_type} = PlatformTypes.create_platform_type(@valid_attrs)
      assert platform_type.name == "some name"
      assert platform_type.uniq_id == "some uniq_id"
    end

    test "create_platform_type/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = PlatformTypes.create_platform_type(@invalid_attrs)
    end

    test "update_platform_type/2 with valid data updates the platform_type" do
      platform_type = platform_type_fixture()
      assert {:ok, platform_type} = PlatformTypes.update_platform_type(platform_type, @update_attrs)
      assert %PlatformType{} = platform_type
      assert platform_type.name == "some updated name"
      assert platform_type.uniq_id == "some updated uniq_id"
    end

    test "update_platform_type/2 with invalid data returns error changeset" do
      platform_type = platform_type_fixture()
      assert {:error, %Ecto.Changeset{}} = PlatformTypes.update_platform_type(platform_type, @invalid_attrs)
      assert platform_type == PlatformTypes.get_platform_type!(platform_type.id)
    end

    test "delete_platform_type/1 deletes the platform_type" do
      platform_type = platform_type_fixture()
      assert {:ok, %PlatformType{}} = PlatformTypes.delete_platform_type(platform_type)
      assert_raise Ecto.NoResultsError, fn -> PlatformTypes.get_platform_type!(platform_type.id) end
    end

    test "change_platform_type/1 returns a platform_type changeset" do
      platform_type = platform_type_fixture()
      assert %Ecto.Changeset{} = PlatformTypes.change_platform_type(platform_type)
    end
  end
end
