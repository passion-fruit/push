defmodule Push.MessagingTest do
  use Push.DataCase

  alias Push.Messaging

  describe "push_notifications" do
    alias Push.Messaging.PushNotification

    @valid_attrs %{}
    @update_attrs %{}
    @invalid_attrs %{}

    def push_notification_fixture(attrs \\ %{}) do
      {:ok, push_notification} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Messaging.create_push_notification()

      push_notification
    end

    test "list_push_notifications/0 returns all push_notifications" do
      push_notification = push_notification_fixture()
      assert Messaging.list_push_notifications() == [push_notification]
    end

    test "get_push_notification!/1 returns the push_notification with given id" do
      push_notification = push_notification_fixture()
      assert Messaging.get_push_notification!(push_notification.id) == push_notification
    end

    test "create_push_notification/1 with valid data creates a push_notification" do
      assert {:ok, %PushNotification{} = push_notification} = Messaging.create_push_notification(@valid_attrs)
    end

    test "create_push_notification/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Messaging.create_push_notification(@invalid_attrs)
    end

    test "update_push_notification/2 with valid data updates the push_notification" do
      push_notification = push_notification_fixture()
      assert {:ok, push_notification} = Messaging.update_push_notification(push_notification, @update_attrs)
      assert %PushNotification{} = push_notification
    end

    test "update_push_notification/2 with invalid data returns error changeset" do
      push_notification = push_notification_fixture()
      assert {:error, %Ecto.Changeset{}} = Messaging.update_push_notification(push_notification, @invalid_attrs)
      assert push_notification == Messaging.get_push_notification!(push_notification.id)
    end

    test "delete_push_notification/1 deletes the push_notification" do
      push_notification = push_notification_fixture()
      assert {:ok, %PushNotification{}} = Messaging.delete_push_notification(push_notification)
      assert_raise Ecto.NoResultsError, fn -> Messaging.get_push_notification!(push_notification.id) end
    end

    test "change_push_notification/1 returns a push_notification changeset" do
      push_notification = push_notification_fixture()
      assert %Ecto.Changeset{} = Messaging.change_push_notification(push_notification)
    end
  end
end
