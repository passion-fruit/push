defmodule PushWeb.PushNotificationControllerTest do
  use PushWeb.ConnCase

  alias Push.Messaging

  @create_attrs %{}
  @update_attrs %{}
  @invalid_attrs %{}

  def fixture(:push_notification) do
    {:ok, push_notification} = Messaging.create_push_notification(@create_attrs)
    push_notification
  end

  describe "index" do
    test "lists all push_notifications", %{conn: conn} do
      conn = get conn, push_notification_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Push notifications"
    end
  end

  describe "new push_notification" do
    test "renders form", %{conn: conn} do
      conn = get conn, push_notification_path(conn, :new)
      assert html_response(conn, 200) =~ "New Push notification"
    end
  end

  describe "create push_notification" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, push_notification_path(conn, :create), push_notification: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == push_notification_path(conn, :show, id)

      conn = get conn, push_notification_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Push notification"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, push_notification_path(conn, :create), push_notification: @invalid_attrs
      assert html_response(conn, 200) =~ "New Push notification"
    end
  end

  describe "edit push_notification" do
    setup [:create_push_notification]

    test "renders form for editing chosen push_notification", %{conn: conn, push_notification: push_notification} do
      conn = get conn, push_notification_path(conn, :edit, push_notification)
      assert html_response(conn, 200) =~ "Edit Push notification"
    end
  end

  describe "update push_notification" do
    setup [:create_push_notification]

    test "redirects when data is valid", %{conn: conn, push_notification: push_notification} do
      conn = put conn, push_notification_path(conn, :update, push_notification), push_notification: @update_attrs
      assert redirected_to(conn) == push_notification_path(conn, :show, push_notification)

      conn = get conn, push_notification_path(conn, :show, push_notification)
      assert html_response(conn, 200)
    end

    test "renders errors when data is invalid", %{conn: conn, push_notification: push_notification} do
      conn = put conn, push_notification_path(conn, :update, push_notification), push_notification: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Push notification"
    end
  end

  describe "delete push_notification" do
    setup [:create_push_notification]

    test "deletes chosen push_notification", %{conn: conn, push_notification: push_notification} do
      conn = delete conn, push_notification_path(conn, :delete, push_notification)
      assert redirected_to(conn) == push_notification_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, push_notification_path(conn, :show, push_notification)
      end
    end
  end

  defp create_push_notification(_) do
    push_notification = fixture(:push_notification)
    {:ok, push_notification: push_notification}
  end
end
