defmodule PushWeb.PushApplicationControllerTest do
  use PushWeb.ConnCase

  alias Push.PushApplications

  @create_attrs %{live_api_key: "some live_api_key", live_secret_key: "some live_secret_key", name: "some name", test_api_key: "some test_api_key", test_secret_key: "some test_secret_key", viewing_test_data: true}
  @update_attrs %{live_api_key: "some updated live_api_key", live_secret_key: "some updated live_secret_key", name: "some updated name", test_api_key: "some updated test_api_key", test_secret_key: "some updated test_secret_key", viewing_test_data: false}
  @invalid_attrs %{live_api_key: nil, live_secret_key: nil, name: nil, test_api_key: nil, test_secret_key: nil, viewing_test_data: nil}

  def fixture(:push_application) do
    {:ok, push_application} = PushApplications.create_push_application(@create_attrs)
    push_application
  end

  describe "index" do
    test "lists all push_applications", %{conn: conn} do
      conn = get conn, push_application_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Push applications"
    end
  end

  describe "new push_application" do
    test "renders form", %{conn: conn} do
      conn = get conn, push_application_path(conn, :new)
      assert html_response(conn, 200) =~ "New Push application"
    end
  end

  describe "create push_application" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, push_application_path(conn, :create), push_application: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == push_application_path(conn, :show, id)

      conn = get conn, push_application_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Push application"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, push_application_path(conn, :create), push_application: @invalid_attrs
      assert html_response(conn, 200) =~ "New Push application"
    end
  end

  describe "edit push_application" do
    setup [:create_push_application]

    test "renders form for editing chosen push_application", %{conn: conn, push_application: push_application} do
      conn = get conn, push_application_path(conn, :edit, push_application)
      assert html_response(conn, 200) =~ "Edit Push application"
    end
  end

  describe "update push_application" do
    setup [:create_push_application]

    test "redirects when data is valid", %{conn: conn, push_application: push_application} do
      conn = put conn, push_application_path(conn, :update, push_application), push_application: @update_attrs
      assert redirected_to(conn) == push_application_path(conn, :show, push_application)

      conn = get conn, push_application_path(conn, :show, push_application)
      assert html_response(conn, 200) =~ "some updated live_api_key"
    end

    test "renders errors when data is invalid", %{conn: conn, push_application: push_application} do
      conn = put conn, push_application_path(conn, :update, push_application), push_application: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Push application"
    end
  end

  describe "delete push_application" do
    setup [:create_push_application]

    test "deletes chosen push_application", %{conn: conn, push_application: push_application} do
      conn = delete conn, push_application_path(conn, :delete, push_application)
      assert redirected_to(conn) == push_application_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, push_application_path(conn, :show, push_application)
      end
    end
  end

  defp create_push_application(_) do
    push_application = fixture(:push_application)
    {:ok, push_application: push_application}
  end
end
