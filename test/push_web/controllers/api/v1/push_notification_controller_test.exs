defmodule PushWeb.API.V1.PushNotificationControllerTest do
  use PushWeb.ConnCase

  alias Push.Messaging
  alias Push.Messaging.PushNotification

  @create_attrs %{}
  @update_attrs %{}
  @invalid_attrs %{}

  def fixture(:push_notification) do
    {:ok, push_notification} = Messaging.create_push_notification(@create_attrs)
    push_notification
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all push_notifications", %{conn: conn} do
      conn = get conn, api_v1_push_notification_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create push_notification" do
    test "renders push_notification when data is valid", %{conn: conn} do
      conn = post conn, api_v1_push_notification_path(conn, :create), push_notification: @create_attrs
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get conn, api_v1_push_notification_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id}
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, api_v1_push_notification_path(conn, :create), push_notification: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update push_notification" do
    setup [:create_push_notification]

    test "renders push_notification when data is valid", %{conn: conn, push_notification: %PushNotification{id: id} = push_notification} do
      conn = put conn, api_v1_push_notification_path(conn, :update, push_notification), push_notification: @update_attrs
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get conn, api_v1_push_notification_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id}
    end

    test "renders errors when data is invalid", %{conn: conn, push_notification: push_notification} do
      conn = put conn, api_v1_push_notification_path(conn, :update, push_notification), push_notification: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete push_notification" do
    setup [:create_push_notification]

    test "deletes chosen push_notification", %{conn: conn, push_notification: push_notification} do
      conn = delete conn, api_v1_push_notification_path(conn, :delete, push_notification)
      assert response(conn, 204)
      assert_error_sent 404, fn ->
        get conn, api_v1_push_notification_path(conn, :show, push_notification)
      end
    end
  end

  defp create_push_notification(_) do
    push_notification = fixture(:push_notification)
    {:ok, push_notification: push_notification}
  end
end
