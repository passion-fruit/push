defmodule Push.Repo.Migrations.CreatePushApplicationsUsers do
  use Ecto.Migration

  def change do
    create table(:push_applications_users) do
      add :user_id, references(:users, on_delete: :nothing), null: false
      add :push_application_id, references(:push_applications, on_delete: :nothing), null: false

      timestamps()
    end

    create index(:push_applications_users, [:user_id])
    create index(:push_applications_users, [:push_application_id])
  end
end
