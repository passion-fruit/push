defmodule Push.Repo.Migrations.CreatePlatformTypes do
  use Ecto.Migration

  def change do
    create table(:platform_types) do
      add :name, :string, null: false
      add :uniq_id, :string, null: false

      timestamps()
    end

    create index(:platform_types, [:uniq_id])
  end
end
