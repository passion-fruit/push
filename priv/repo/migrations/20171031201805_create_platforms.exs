defmodule Push.Repo.Migrations.CreatePlatforms do
  use Ecto.Migration

  def change do
    create table(:platforms) do
      add :ios_bundle_id, :string
      add :ios_cert, :string, size: 3000
      add :ios_key, :string, size: 3000
      add :push_application_id, references(:push_applications, on_delete: :nothing), null: false
      add :platform_type_id, references(:platform_types, on_delete: :nothing), null: false

      timestamps()
    end

    create index(:platforms, [:push_application_id])
    create index(:platforms, [:platform_type_id])
  end
end
