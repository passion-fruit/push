defmodule Push.Repo.Migrations.CreateGettingStarteds do
  use Ecto.Migration

  def change do
    create table(:getting_starteds) do
      add :push_application_id, references(:push_applications, on_delete: :nothing), null: false
      add :step, :string

      timestamps()
    end

  end
end
