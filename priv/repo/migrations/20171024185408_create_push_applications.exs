defmodule Push.Repo.Migrations.CreatePushApplications do
  use Ecto.Migration

  def change do
    create table(:push_applications) do
      add :name, :string, null: false
      add :test_api_key, :string, null: false
      add :test_secret_key, :string, null: false
      add :viewing_test_data, :boolean, default: false, null: false
      add :live_api_key, :string, null: false
      add :live_secret_key, :string, null: false

      add :getting_started_step, :string, null: false

      timestamps()
    end

  end
end
