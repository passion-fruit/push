defmodule Push.Repo.Migrations.CreatePushNotifications do
  use Ecto.Migration

  def change do
    create table(:push_notifications) do
      add :push_application_id, references(:push_applications, on_delete: :nothing), null: false
      add :platform_id, references(:platforms, on_delete: :nothing), null: false
      add :app_environment, :string, null: false
      add :message, :string, null: false
      add :device_push_tokens, { :array, :string }, null: false

      timestamps()
    end

    create index(:push_notifications, [:push_application_id])
  end
end
