# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Push.Repo.insert!(%Push.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

Push.Repo.insert!(%Push.PlatformTypes.PlatformType{
  name: "iOS",
  uniq_id: "ios"
  })

Push.Repo.insert!(%Push.PlatformTypes.PlatformType{
  name: "Android",
  uniq_id: "android"
  })

Push.Repo.insert!(%Push.PlatformTypes.PlatformType{
  name: "Web",
  uniq_id: "web"
  })