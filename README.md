# Push Horse

Code coverage
[![coverage report](https://gitlab.com/twizlor/push/badges/master/coverage.svg)](https://gitlab.com/twizlor/push/commits/master)


To start the application:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.