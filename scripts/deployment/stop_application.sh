#!/usr/bin/env bash

echo "Stopping application"

if [ -d /home/ubuntu/push ]
then
    echo "App directory exists"

    sudo /home/ubuntu/push/bin/push stop
    sudo rm -rf /home/ubuntu/push
fi
