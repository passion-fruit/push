### Build ======================================================================


# Alias this container as builder:
FROM bitwalker/alpine-elixir-phoenix as builder

# These args are passed in by the docker build command.
# Example:
# docker build -t push:latest \
#   --build-arg PHOENIX_SECRET_KEY_BASE=super_secret_phoenix_key_base \
#   --build-arg SESSION_COOKIE_NAME=session_cookie_name \
#   --build-arg SESSION_COOKIE_SIGNING_SALT=super_secret_cookie_signing_salt \
#   --build-arg SESSION_COOKIE_ENCRYPTION_SALT=super_secret_cookie_encryption_salt \
#   --build-arg DATABASE_URL=postgresql://postgres:postgres@localhost/push_prod \
#   .
ARG SECRET_KEY_BASE
ARG DATABASE_URL
ARG SENTRY_API_URL
ARG MAILGUN_API_KEY
ARG MAILGUN_DOMAIN
ARG AWS_ACCESS_KEY_ID
ARG AWS_SECRET_ACCESS_KEY
ARG BUCKET_NAME

# Assign the ARGs from the docker build command to the ENV vars in the container.
ENV MIX_ENV=prod \
    SECRET_KEY_BASE=$SECRET_KEY_BASE \
    DATABASE_URL=$DATABASE_URL \
    SENTRY_API_URL=$SENTRY_API_URL \
    MAILGUN_API_KEY=$MAILGUN_API_KEY \
    MAILGUN_DOMAIN=$MAILGUN_DOMAIN \
    AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
    AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
    BUCKET_NAME=$BUCKET_NAME

WORKDIR /push

COPY ./ ./

RUN mix do deps.get, deps.compile

WORKDIR /push/assets

RUN npm install && ./node_modules/brunch/bin/brunch build --production

WORKDIR /push

RUN mix phx.digest

RUN mix release --env=prod --verbose


### Release ====================================================================


# TODO Check for docker images from AWS.
FROM alpine:3.6

# We need bash and openssl for Phoenix
RUN apk upgrade --no-cache && \
    apk add --no-cache bash openssl

EXPOSE 4000
EXPOSE 5432

ENV MIX_ENV=prod \
    SHELL=/bin/bash

WORKDIR /push

# Copy the built tarball from the previous 'builder' stage.
COPY --from=builder /push/_build/prod/rel/push/releases/0.0.1/push.tar.gz .

RUN tar zxf push.tar.gz && rm push.tar.gz

CMD ["/push/bin/push", "foreground"]