defmodule PushWeb.Email do
  import Bamboo.Email
  import Bamboo.Phoenix

  use Bamboo.Phoenix, view: PushWeb.EmailView

  def confirmation_email(conn, user) do
    base_email
    |> to(user.email)
    |> from("me@example.com")
    |> subject("Push Horse - Confirm Your Email")
    |> assign(:user, user)
    |> assign(:conn, conn)
    |> render("confirmation.html")
  end

  defp base_email do
    new_email
    |> put_html_layout({PushWeb.LayoutView, "email.html"})
  end
end
