defmodule PushWeb.Plugs.FetchCurrentApplication do
  import Plug.Conn
  alias Push.PushApplications

  def fetch_current_application(conn, _params) do
    application = PushApplications.get_push_application!(get_session(conn, :current_application_id))
    %{ conn | :assigns => Map.put(conn.assigns, :current_application, application) }
  end
end
