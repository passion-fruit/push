defmodule PushWeb.Plugs.GettingStarted do
  import Plug.Conn
  import Phoenix.Controller
  alias Push.GettingStarteds
  alias PushWeb.Router.Helpers
  alias Push.PushApplications

  def redirect_to_getting_started_if_required(conn, _params) do
    if conn.request_path == Helpers.getting_started_path(conn, :index) do
      conn
    else
      current_application = PushApplications.get_push_application!(get_session(conn, :current_application_id))

      if current_application.getting_started.step != "completed" do
        conn
        |> redirect(to: Helpers.getting_started_path(conn, :index))
        |> halt()
      else
        conn
      end
    end
  end
end
