defmodule PushWeb.Router do
  use PushWeb, :router
  use Plug.ErrorHandler
  use Sentry.Plug

  import PushWeb.Plugs.Authentication
  import PushWeb.Plugs.GettingStarted
  import PushWeb.Plugs.FetchCurrentApplication

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :protected do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :authenticate_user
    plug :fetch_current_application
    plug :redirect_to_getting_started_if_required

    plug :put_layout, {PushWeb.LayoutView, :logged_in}
  end

  scope "/", PushWeb do
    pipe_through :browser # Use the default browser stack
    # Add public routes below.
    get "/", StaticPageController, :home

    resources "/sessions", SessionController, only: [:new, :create]
    delete "/signout", SessionController, :delete

    resources "/registrations", RegistrationController, only: [:new, :create]
  end

  scope "/", PushWeb do
    pipe_through :protected

    resources "/users", UserController
    resources "/dashboard", DashboardController, only: [:index]
    resources "/push_notifications", PushNotificationController, only: [:new, :create]
    resources "/confirmation", ConfirmationController, only: [:create, :show]
    resources "/getting_started", GettingStartedController, only: [:index, :create]
  end

  scope "/push_application", PushWeb do
    pipe_through :protected

    resources "/platforms", PlatformController, only: [:index, :new, :create]

    get "/keys", PushApplicationController, :keys
    resources "/", PushApplicationController
  end

  #################################################################################
  # API routes
  #################################################################################
  pipeline :api do
    plug :accepts, ["json"]
    plug BasicAuth, callback: &Push.PushApplications.find_by_key_and_secret/3
  end


  scope "/api/v1", PushWeb.API.V1, as: :api_v1 do
    pipe_through :api

    resources "/push_notifications", PushNotificationController, only: [:create]
  end
end
