defmodule PushWeb.GettingStartedView do
  use PushWeb, :view

  def platform_type_icon(platform_type) do
    case platform_type.uniq_id do
      "ios" -> "fa-apple"
      "android" -> "fa-android"
      "web" -> "fa-code"
    end
  end
end
