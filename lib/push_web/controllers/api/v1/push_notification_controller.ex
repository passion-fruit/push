defmodule PushWeb.API.V1.PushNotificationController do
  use PushWeb, :controller

  alias Push.Messaging
  alias Push.Messaging.PushNotification

  action_fallback PushWeb.API.FallbackController

  def create(conn, %{"push_notification" => push_notification_params}) do
    %{
      "device_push_token" => device_push_token,
      "message" => message
    } = push_notification_params

    attrs = %{
      device_push_tokens: [device_push_token],
      message: message,
      app_environment: conn.assigns.current_application_env,
      platform_id: 1,
      push_application_id: conn.assigns.current_application_id # the current application
    }

    with {:ok, %PushNotification{} = _push_notification} <- Push.Messaging.create_push_notification(attrs) do
       send_resp(conn, 202, "")
    end
  end
end
