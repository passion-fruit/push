defmodule PushWeb.PlatformController do
  use PushWeb, :controller

  alias Push.Platforms
  alias Push.Platforms.Platform

  def index(conn, _params) do
    current_application = Push.PushApplications.get_push_application!(get_session(conn, :current_application_id))
    platform_types = Push.PlatformTypes.list_platform_types
    render(conn, "index.html", platforms: current_application.platforms, platform_types: platform_types)
  end

  def new(conn, params) do
    changeset = Platforms.change_platform(%Platform{})
    render(conn, "new.html", changeset: changeset, platform_type_id: params["platform_type_id"])
  end

  def create(conn, %{"platform" => platform_params}) do
    # Add the push application ID to the params.
    platform_params = Map.put(platform_params,
                                  "push_application_id",
                                  get_session(conn, :current_application_id))

    case Platforms.create_platform(platform_params) do
      {:ok, platform} ->
        conn
        |> put_flash(:info, "Platform created successfully.")
        |> redirect(to: platform_path(conn, :index))
      {:error, %Ecto.Changeset{} = changeset} ->
        IO.inspect changeset
        render(conn, "new.html", changeset: changeset, platform_type_id: platform_params["platform_type_id"])
    end
  end
end
