defmodule PushWeb.PushNotificationController do
  use PushWeb, :controller

  alias Push.Messaging
  alias Push.Messaging.PushNotification

  def new(conn, _params) do
    application_platforms = Push.PushApplications.get_push_application!(get_session(conn, :current_application_id)).platforms
    IO.inspect application_platforms
    render conn, "new.html", application_platforms: application_platforms
  end

  def create(conn, params) do
    # device_tokens: ["9c8fed0b075da196bbbe5a8bdd934f421de5d1133c89e58e43db1de288282803"],
    %{
      "notification" => %{
        "device_push_token" => device_push_token,
        "message" => message
      }
    } = params

    attrs = %{
      device_push_tokens: [device_push_token],
      message: message,
      app_environment: "dev",
      platform_id: 1,
      push_application_id: get_session(conn, :current_application_id)
    }

    case Push.Messaging.create_push_notification(attrs) do
      {:ok, push_notification} ->
        conn
        |> put_flash(:info, "Push Notification created.")
        |> redirect(to: push_notification_path(conn, :new))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end
end
