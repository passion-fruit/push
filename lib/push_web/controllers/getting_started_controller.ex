defmodule PushWeb.GettingStartedController do
  use PushWeb, :controller
  use Ecto.Schema

  alias Push.PushApplications

  @moduledoc """
  """

  def index(conn, _params) do
    platform_types = Push.PlatformTypes.list_platform_types
    step_template = "#{conn.assigns.current_application.getting_started.step}.html"
    render conn, step_template, platform_types: platform_types
  end

  def create(conn, _params) do
  end
end
