defmodule PushWeb.StaticPageController do
  use PushWeb, :controller

  plug :put_layout, false

  def home(conn, _params) do
    render conn, "home.html"
  end
end
