defmodule PushWeb.DashboardController do
  use PushWeb, :controller
  use Ecto.Schema

  alias Push.PushApplicationsUsers
  alias Push.PushApplications

  @moduledoc """
  """

  def index(conn, _params) do
    #TODO: Why does the line below not work?
    #Push.PushApplicationsUsers.PushApplicationUser |> where(user_id: ^user_id) |> Repo.all

    applications = Push.Accounts.User.applications_for_user(conn.assigns[:current_user])
    render conn, "index.html", applications: applications
  end
end
