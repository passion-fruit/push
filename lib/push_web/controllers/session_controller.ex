defmodule PushWeb.SessionController do
  use PushWeb, :controller

  alias Push.Accounts
  alias Push.Accounts.User
  alias Comeonin.Bcrypt

  def new(conn, _params) do
    render(conn, "new.html")
  end

  def create(conn, %{ "session" => params}) do
    potential_user = Accounts.get_user_by_email(params["email"])

    case Bcrypt.check_pass(potential_user, params["password"]) do
      {:ok, user} ->
        [push_app|_] = Push.Accounts.User.applications_for_user(user)

        conn
        |> put_session(:current_application_id, push_app.id)
        |> put_session(:current_user_id, user.id)
        |> redirect(to: dashboard_path(conn, :index))
     {:error, user} ->
        render(conn, "new.html")
    end
  end

  def delete(conn, _params) do
    conn
    |> delete_session(:current_user_id)
    |> redirect(to: static_page_path(conn, :home))
  end
end
