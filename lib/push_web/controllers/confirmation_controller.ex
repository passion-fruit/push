defmodule PushWeb.ConfirmationController do
  use PushWeb, :controller

  @token_salt "confirmation_salt_123123123"

  def create(conn, _params) do
    new_token = Phoenix.Token.sign(PushWeb.Endpoint, @token_salt, conn.assigns.current_user.id)

    {:ok, user} = Push.Accounts.update_user(conn.assigns.current_user, %{confirmation_token: new_token})

    PushWeb.Email.confirmation_email(conn, user)
    |> PushWeb.Mailer.deliver_later

    conn
    |> redirect(to: dashboard_path(conn, :index))
  end

  def show(conn, %{ "id" => token }) do
    with {:ok, user_id} <- Phoenix.Token.verify(PushWeb.Endpoint, @token_salt, token, max_age: 86_400),
         {:ok, user} <- Push.Accounts.confirm!(user_id) do
      conn
      |> put_flash(:info, "Thank you for confirming your account. You can now switch 'viewing testing data' on and off.")
      |> redirect(to: dashboard_path(conn, :index))
    end
  end
end
