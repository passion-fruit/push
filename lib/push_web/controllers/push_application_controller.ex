defmodule PushWeb.PushApplicationController do
  use PushWeb, :controller

  alias Push.PushApplications
  alias Push.PushApplications.PushApplication

  def index(conn, _params) do
    push_applications = PushApplications.list_push_applications()
    render(conn, "index.html", push_applications: push_applications)
  end

  def new(conn, _params) do
    changeset = PushApplications.change_push_application(%PushApplication{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"push_application" => push_application_params}) do
    case PushApplications.create_push_application(push_application_params) do
      {:ok, push_application} ->
        conn
        |> put_flash(:info, "Push application created successfully.")
        |> redirect(to: push_application_path(conn, :show, push_application))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    push_application = PushApplications.get_push_application!(id)
    render(conn, "show.html", push_application: push_application)
  end

  def edit(conn, %{"id" => id}) do
    push_application = PushApplications.get_push_application!(id)
    changeset = PushApplications.change_push_application(push_application)
    render(conn, "edit.html", push_application: push_application, changeset: changeset)
  end

  def update(conn, %{"id" => id, "push_application" => push_application_params}) do
    push_application = PushApplications.get_push_application!(id)

    case PushApplications.update_push_application(push_application, push_application_params) do
      {:ok, push_application} ->
        conn
        |> put_flash(:info, "Push application updated successfully.")
        |> redirect(to: push_application_path(conn, :show, push_application))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", push_application: push_application, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    push_application = PushApplications.get_push_application!(id)
    {:ok, _push_application} = PushApplications.delete_push_application(push_application)

    conn
    |> put_flash(:info, "Push application deleted successfully.")
    |> redirect(to: push_application_path(conn, :index))
  end


  def keys(conn, _params) do
    push_application = PushApplications.get_push_application!(get_session(conn, :current_application_id))
    render(conn, "keys.html", push_application: push_application)
  end
end
