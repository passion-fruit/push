defmodule Push.GettingStarteds.GettingStarted do
  use Ecto.Schema
  import Ecto.Changeset

  @platform "platforms"
  @authentication "authentication"
  @test_notification "test_notification"
  @completed "completed"
  @steps [@platform, @authentication, @test_notification, @completed]

  schema "getting_starteds" do
    field :step, :string, default: @platform

    belongs_to :push_application, Push.PushApplications.PushApplication

    timestamps()
  end

  @doc false
  def changeset(getting_started, attrs) do
    getting_started
    |> cast(attrs, [:step])
    |> validate_required([:step])
  end
end
