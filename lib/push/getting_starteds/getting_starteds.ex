defmodule Push.GettingStarteds do
  @moduledoc """
  The GettingStarteds context.
  """

  import Ecto.Query, warn: false
  alias Push.Repo

  alias Push.GettingStarteds.GettingStarted

  @doc """
  Returns the list of getting_starteds.

  ## Examples

      iex> list_getting_starteds()
      [%GettingStarted{}, ...]

  """
  def list_getting_starteds do
    Repo.all(GettingStarted)
  end

  @doc """
  Gets a single getting_started.

  Raises `Ecto.NoResultsError` if the Getting started does not exist.

  ## Examples

      iex> get_getting_started!(123)
      %GettingStarted{}

      iex> get_getting_started!(456)
      ** (Ecto.NoResultsError)

  """
  def get_getting_started!(id), do: Repo.get!(GettingStarted, id)

  @doc """
  Creates a getting_started.

  ## Examples

      iex> create_getting_started(%{field: value})
      {:ok, %GettingStarted{}}

      iex> create_getting_started(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_getting_started(attrs \\ %{}) do
    %GettingStarted{}
    |> GettingStarted.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a getting_started.

  ## Examples

      iex> update_getting_started(getting_started, %{field: new_value})
      {:ok, %GettingStarted{}}

      iex> update_getting_started(getting_started, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_getting_started(%GettingStarted{} = getting_started, attrs) do
    getting_started
    |> GettingStarted.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a GettingStarted.

  ## Examples

      iex> delete_getting_started(getting_started)
      {:ok, %GettingStarted{}}

      iex> delete_getting_started(getting_started)
      {:error, %Ecto.Changeset{}}

  """
  def delete_getting_started(%GettingStarted{} = getting_started) do
    Repo.delete(getting_started)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking getting_started changes.

  ## Examples

      iex> change_getting_started(getting_started)
      %Ecto.Changeset{source: %GettingStarted{}}

  """
  def change_getting_started(%GettingStarted{} = getting_started) do
    GettingStarted.changeset(getting_started, %{})
  end

  def complete?(%GettingStarted{} = getting_started) do
    
  end
end
