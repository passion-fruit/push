defmodule Push.Repo do
  use Ecto.Repo, otp_app: :push

  def init(_, opts) do
    {:ok, opts}
  end
end
