defmodule Push.Platforms do
  @moduledoc """
  The Platforms context.
  """

  import Ecto.Query, warn: false
  alias Push.Repo

  alias Push.Platforms.Platform

  @doc """
  Returns the list of platforms.

  ## Examples

      iex> list_platforms()
      [%Platform{}, ...]

  """
  def list_platforms(push_application_id) do
    Repo.all(Platform)
  end

  @doc """
  Gets a single platform.

  Raises `Ecto.NoResultsError` if the Platform does not exist.

  ## Examples

      iex> get_platform!(123)
      %Platform{}

      iex> get_platform!(456)
      ** (Ecto.NoResultsError)

  """
  def get_platform!(id), do: Repo.get!(Platform, id)

  @doc """
  Creates a platform.

  ## Examples

      iex> create_platform(%{field: value})
      {:ok, %Platform{}}

      iex> create_platform(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_platform(attrs \\ %{}) do
    ios_bundle_id = Push.Platforms.P12Parser.bundle_id(
      attrs["p12_file_path"].path, attrs["p12_password"])
    ios_cert_str = Push.Platforms.P12Parser.cert_pem(
      attrs["p12_file_path"].path, attrs["p12_password"], attrs["push_application_id"])
    ios_key_str = Push.Platforms.P12Parser.key_pem(
      attrs["p12_file_path"].path, attrs["p12_password"], attrs["push_application_id"])

    attrs = Map.put(attrs, "ios_cert", ios_cert_str)
    attrs = Map.put(attrs, "ios_key", ios_key_str)
    attrs = Map.put(attrs, "ios_bundle_id", ios_bundle_id)

    IO.inspect(attrs)

    %Platform{}
    |> Platform.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a platform.

  ## Examples

      iex> update_platform(platform, %{field: new_value})
      {:ok, %Platform{}}

      iex> update_platform(platform, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_platform(%Platform{} = platform, attrs) do
    platform
    |> Platform.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Platform.

  ## Examples

      iex> delete_platform(platform)
      {:ok, %Platform{}}

      iex> delete_platform(platform)
      {:error, %Ecto.Changeset{}}

  """
  def delete_platform(%Platform{} = platform) do
    Repo.delete(platform)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking platform changes.

  ## Examples

      iex> change_platform(platform)
      %Ecto.Changeset{source: %Platform{}}

  """
  def change_platform(%Platform{} = platform) do
    Platform.changeset(platform, %{})
  end

  alias Push.Platforms.Platform
  alias Push.PlatformTypes.PlatformType

  @doc """
  Creates a ios_platform.

  ## Examples

      iex> create_ios_platform(%{field: value})
      {:ok, %IosPlatform{}}

      iex> create_ios_platform(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  # def create_ios_platform(attrs \\ %{}) do
  #   {:ok, file_binary} = File.read(attrs["p12_file_path"].path)

  #   bucket_name = System.get_env["BUCKET_NAME"] || "${BUCKET_NAME}"

  #   s3_path = "#{Mix.env}/#{attrs["push_application_id"]}/#{attrs["p12_file_path"].filename}"

  #   attrs = Map.update!(attrs,
  #                       "p12_file_path",
  #                      fn _value -> "https://#{bucket_name}.s3.amazonaws.com/#{bucket_name}/#{s3_path}" end)

  #   image = ExAws.S3.put_object(bucket_name, s3_path, file_binary)
  #           |> ExAws.request!

  #   %IosPlatform{}
  #   |> IosPlatform.changeset(attrs)
  #   |> Repo.insert()
  #   end
end
