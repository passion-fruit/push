defmodule Push.Platforms.P12Parser do

  def bundle_id(p12_cert, cert_password) do
    {certificate_str, _exit_code} = System.cmd("openssl", ["pkcs12", "-in", p12_cert, "-passin", "pass:123123123", "-nokeys"])

    # Get the line with the bundle Id in it.
    # It's the 4th line of the outputed string.
    bundle_id_line = certificate_str
                     |> String.split("\n")
                     |> Enum.at(3)

    # bundle_id_line looks like this:
    # "subject=/UID=com.myapp/CN=Apple Push Services: com.myapp/OU=123123123/O=Joe Bloggs/C=US"
    bundle_id = bundle_id_line
                |> String.split("/")
                |> Enum.at(1)
                |> String.split("=")
                |> List.last
    bundle_id
  end

  def cert_pem(p12_cert, cert_password, push_app_id) do
    :os.cmd(:"openssl pkcs12 -in #{p12_cert} -passin pass:#{cert_password} -nokeys")
    |> List.to_string
    |> String.replace("MAC verified OK\n", "")
  end

  def key_pem(p12_cert, cert_password, push_app_id) do
    :os.cmd(:"openssl pkcs12 -in #{p12_cert} -passin pass:#{cert_password} -nocerts -nodes | openssl rsa")
    |> List.to_string
    |> String.replace("MAC verified OK\n", "")
  end
end