defmodule Push.Platforms.Platform do
  use Ecto.Schema
  import Ecto.Changeset
  alias Push.Platforms.Platform


  schema "platforms" do
    field :ios_bundle_id, :string
    field :ios_cert, :string
    field :ios_key, :string

    belongs_to :platform_type, Push.PlatformTypes.PlatformType
    belongs_to :push_application, Push.PushApplications.PushApplication

    timestamps()
  end

  @doc false
  def changeset(%Platform{} = platform, attrs) do
    platform
    |> cast(attrs, [:ios_bundle_id, :ios_cert, :ios_key, :platform_type_id, :push_application_id])
    |> validate_required([:ios_bundle_id, :ios_cert, :ios_key, :platform_type_id, :push_application_id])
  end
end
