defmodule Push.Messaging do
  @moduledoc """
  The Messaging context.
  """

  import Ecto.Query, warn: false
  alias Push.Repo

  alias Push.Messaging.PushNotification

  @doc """
  Returns the list of push_notifications.

  ## Examples

      iex> list_push_notifications()
      [%PushNotification{}, ...]

  """
  def list_push_notifications do
    Repo.all(PushNotification)
  end

  @doc """
  Gets a single push_notification.

  Raises `Ecto.NoResultsError` if the Push notification does not exist.

  ## Examples

      iex> get_push_notification!(123)
      %PushNotification{}

      iex> get_push_notification!(456)
      ** (Ecto.NoResultsError)

  """
  def get_push_notification!(id), do: Repo.get!(PushNotification, id)

  @doc """
  Creates a push_notification.

  ## Examples

      iex> create_push_notification(%{field: value})
      {:ok, %PushNotification{}}

      iex> create_push_notification(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_push_notification(attrs \\ %{}) do
    inserted_result = %PushNotification{}
                      |> PushNotification.changeset(attrs)
                      |> Repo.insert()

    case inserted_result do
      {:ok, push_notification} ->
        platform = Repo.one(Ecto.assoc(push_notification, :platform))

        # These are the parameters that the Notifications name space part of the
        # application needs to successfully send push notifications.
        request = %{
          app_id: push_notification.push_application_id,
          platform_id: push_notification.platform_id,
          payload: %{},
          env: push_notification.app_environment,
          device_tokens: push_notification.device_push_tokens,
          bundle_id: platform.ios_bundle_id,
          msg: push_notification.message,
          ios_cert: platform.ios_cert,
          ios_key: platform.ios_key,
        }

        Notifications.NotificationManager.start_link
        Notifications.NotificationManager.request(request)

        {:ok, push_notification}
      {:error, changeset} ->
        {:error, changeset}
    end


  end

  @doc """
  Updates a push_notification.

  ## Examples

      iex> update_push_notification(push_notification, %{field: new_value})
      {:ok, %PushNotification{}}

      iex> update_push_notification(push_notification, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_push_notification(%PushNotification{} = push_notification, attrs) do
    push_notification
    |> PushNotification.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a PushNotification.

  ## Examples

      iex> delete_push_notification(push_notification)
      {:ok, %PushNotification{}}

      iex> delete_push_notification(push_notification)
      {:error, %Ecto.Changeset{}}

  """
  def delete_push_notification(%PushNotification{} = push_notification) do
    Repo.delete(push_notification)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking push_notification changes.

  ## Examples

      iex> change_push_notification(push_notification)
      %Ecto.Changeset{source: %PushNotification{}}

  """
  def change_push_notification(%PushNotification{} = push_notification) do
    PushNotification.changeset(push_notification, %{})
  end
end
