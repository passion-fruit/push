defmodule Push.Messaging.PushNotification do
  use Ecto.Schema
  import Ecto.Changeset
  alias Push.Messaging.PushNotification


  schema "push_notifications" do
    field :app_environment, :string, null: false
    field :message, :string, null: false
    field :device_push_tokens, { :array, :string }, null: false

    belongs_to :push_application, Push.PushApplications.PushApplication
    belongs_to :platform, Push.Platforms.Platform

    timestamps()
  end

  @doc false
  def changeset(%PushNotification{} = push_notification, attrs) do
    push_notification
    |> cast(attrs, [:push_application_id, :platform_id, :app_environment, :message, :device_push_tokens])
    |> validate_required([:push_application_id, :platform_id, :app_environment, :message, :device_push_tokens])
    |> validate_inclusion(:app_environment, ["dev", "prod"])
  end
end
