defmodule Push.PlatformTypes.PlatformType do
  use Ecto.Schema
  import Ecto.Changeset
  alias Push.PlatformTypes.PlatformType


  schema "platform_types" do
    field :name, :string
    field :uniq_id, :string

    timestamps()
  end

  @doc false
  def changeset(%PlatformType{} = platform_type, attrs) do
    platform_type
    |> cast(attrs, [:name, :uniq_id])
    |> validate_required([:name, :uniq_id])
  end
end
