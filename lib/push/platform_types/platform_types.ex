defmodule Push.PlatformTypes do
  @moduledoc """
  The PlatformTypes context.
  """

  import Ecto.Query, warn: false
  alias Push.Repo

  alias Push.PlatformTypes.PlatformType

  @doc """
  Returns the list of platform_types.

  ## Examples

      iex> list_platform_types()
      [%PlatformType{}, ...]

  """
  def list_platform_types do
    Repo.all(PlatformType)
  end

  @doc """
  Gets a single platform_type.

  Raises `Ecto.NoResultsError` if the Platform type does not exist.

  ## Examples

      iex> get_platform_type!(123)
      %PlatformType{}

      iex> get_platform_type!(456)
      ** (Ecto.NoResultsError)

  """
  def get_platform_type!(id), do: Repo.get!(PlatformType, id)

  @doc """
  Creates a platform_type.

  ## Examples

      iex> create_platform_type(%{field: value})
      {:ok, %PlatformType{}}

      iex> create_platform_type(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_platform_type(attrs \\ %{}) do
    %PlatformType{}
    |> PlatformType.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a platform_type.

  ## Examples

      iex> update_platform_type(platform_type, %{field: new_value})
      {:ok, %PlatformType{}}

      iex> update_platform_type(platform_type, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_platform_type(%PlatformType{} = platform_type, attrs) do
    platform_type
    |> PlatformType.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a PlatformType.

  ## Examples

      iex> delete_platform_type(platform_type)
      {:ok, %PlatformType{}}

      iex> delete_platform_type(platform_type)
      {:error, %Ecto.Changeset{}}

  """
  def delete_platform_type(%PlatformType{} = platform_type) do
    Repo.delete(platform_type)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking platform_type changes.

  ## Examples

      iex> change_platform_type(platform_type)
      %Ecto.Changeset{source: %PlatformType{}}

  """
  def change_platform_type(%PlatformType{} = platform_type) do
    PlatformType.changeset(platform_type, %{})
  end
end
