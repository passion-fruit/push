defmodule Push.PushApplications do
  @moduledoc """
  The PushApplications context.
  """

  import Ecto.Query, warn: false
  alias Push.Repo

  alias Push.PushApplications.PushApplication

  @doc """
  Returns the list of push_applications.

  ## Examples

      iex> list_push_applications()
      [%PushApplication{}, ...]

  """
  def list_push_applications do
    Repo.all(PushApplication)
  end

  @doc """
  Gets a single push_application.

  Raises `Ecto.NoResultsError` if the Push application does not exist.

  ## Examples

      iex> get_push_application!(123)
      %PushApplication{}

      iex> get_push_application!(456)
      ** (Ecto.NoResultsError)

  """
  def get_push_application!(id) do
    PushApplication
      |> Repo.get!(id)
      |> Repo.preload(:getting_started, platforms: :platform_type)
  end

  @doc """
  Creates a push_application.

  ## Examples

      iex> create_push_application(%{field: value})
      {:ok, %PushApplication{}}

      iex> create_push_application(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_push_application(attrs \\ %{}) do
    %PushApplication{}
    |> PushApplication.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a push_application.

  ## Examples

      iex> update_push_application(push_application, %{field: new_value})
      {:ok, %PushApplication{}}

      iex> update_push_application(push_application, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_push_application(%PushApplication{} = push_application, attrs) do
    push_application
    |> PushApplication.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a PushApplication.

  ## Examples

      iex> delete_push_application(push_application)
      {:ok, %PushApplication{}}

      iex> delete_push_application(push_application)
      {:error, %Ecto.Changeset{}}

  """
  def delete_push_application(%PushApplication{} = push_application) do
    Repo.delete(push_application)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking push_application changes.

  ## Examples

      iex> change_push_application(push_application)
      %Ecto.Changeset{source: %PushApplication{}}

  """
  def change_push_application(%PushApplication{} = push_application) do
    PushApplication.changeset(push_application, %{})
  end

  @doc """
    This function looks up the application based on the secret_key and api_key.
    Since a push application uses two sets of keys for distinguishing live and dev environments
    we need to check if the parameters match one set.
  """
  def find_by_key_and_secret(conn, api_key, api_secret) do
    alias Push.PushApplications.PushApplicationEnvironment

    dev_env_application = Repo.get_by(PushApplication, test_api_key: api_key, test_secret_key: api_secret)

    if dev_env_application do
      conn
        |> Plug.Conn.assign(:current_application_id, dev_env_application.id)
        |> Plug.Conn.assign(:current_application_env, PushApplicationEnvironment.assign_dev)
    else
      live_env_application = Repo.get_by(PushApplication, live_api_key: api_key, live_secret_key: api_secret)

      if live_env_application do
        conn
          |> Plug.Conn.assign(:current_application_id, live_env_application.id)
          |> Plug.Conn.assign(:current_application_env, PushApplicationEnvironment.assign_prod)
      else
        Plug.Conn.halt(conn)
      end
    end
  end
end
