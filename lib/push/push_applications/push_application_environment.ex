defmodule Push.PushApplications.PushApplicationEnvironment do
  @moduledoc """
  This module is used to handle interrogating the push application environment.

  When a request comes from the API it sets the application environment based on the api key and secret
  that successfully returned the application.

  If the set of keys was the "dev" set of keys then the application environment is dev/test.
  If the set of keys was the "prod" set of keys then the application environment is production.

  This information is important because it's stored on the push_notification record
  and used to inform the platforms whether the notification is for test purposes to deliver to
  test devices or for non test devices (Real people).
  """

  @dev "dev"
  @prod "prod"

  def assign_dev do
    @dev
  end

  def dev?(env) do
    env == @dev
  end

  def assign_prod do
    @prod
  end

  def prod?(env) do
    env == @prod
  end

  def application_env(possible_env) do
    cond do
      dev?(possible_env) -> @dev
      prod?(possible_env) -> @prod
      true -> raise "Unknown application environment #{possible_env}"
    end
  end
end