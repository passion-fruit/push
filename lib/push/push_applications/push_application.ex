defmodule Push.PushApplications.PushApplication do
  use Ecto.Schema
  import Ecto.Changeset
  import Push.PushApplications.Utils # This is for the generate_key function.

  alias Push.PushApplications.PushApplication

  schema "push_applications" do
    field :live_api_key, :string, default: "live_ak_#{generate_key()}"
    field :live_secret_key, :string, default: "live_sk_#{generate_key()}"
    field :name, :string, default: "Unnamed Application"
    field :test_api_key, :string, default: "test_ak_#{generate_key()}"
    field :test_secret_key, :string, default: "test_sk_#{generate_key()}"
    field :viewing_test_data, :boolean, default: false

    many_to_many :users, Push.Accounts.User, join_through: "push_applications_users"
    has_many :platforms, Push.Platforms.Platform
    has_one :getting_started, Push.GettingStarteds.GettingStarted

    timestamps()
  end

  @doc false
  def changeset(%PushApplication{} = push_application, attrs) do
    push_application
    |> cast(attrs, [:name, :test_api_key, :test_secret_key, :viewing_test_data, :live_api_key, :live_secret_key])
    |> validate_required([:name, :test_api_key, :test_secret_key, :viewing_test_data, :live_api_key, :live_secret_key])
  end

end
