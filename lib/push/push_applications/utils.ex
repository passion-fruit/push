defmodule Push.PushApplications.Utils do
  @doc """
    Ggenerates URL safe hmac string.
  """
  def generate_key do
    :crypto.hash(:md5, Time.utc_now |> Time.to_string) |> Base.encode16 |> String.downcase |> String.slice(0..31)
  end
end
