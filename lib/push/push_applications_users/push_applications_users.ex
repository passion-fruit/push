defmodule Push.PushApplicationsUsers do
  @moduledoc """
  The PushApplicationsUsers context.
  """

  import Ecto.Query, warn: false
  alias Push.Repo

  alias Push.PushApplicationsUsers.PushApplicationUser

  @doc """
  Returns the list of push_applications_users.

  ## Examples

      iex> list_push_applications_users()
      [%PushApplicationUser{}, ...]

  """
  def list_push_applications_users do
    Repo.all(PushApplicationUser)
  end

  @doc """
  Gets a single push_application_user.

  Raises `Ecto.NoResultsError` if the Push application user does not exist.

  ## Examples

      iex> get_push_application_user!(123)
      %PushApplicationUser{}

      iex> get_push_application_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_push_application_user!(id), do: Repo.get!(PushApplicationUser, id)

  @doc """
  Creates a push_application_user.

  ## Examples

      iex> create_push_application_user(%{field: value})
      {:ok, %PushApplicationUser{}}

      iex> create_push_application_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_push_application_user(attrs \\ %{}) do
    %PushApplicationUser{}
    |> PushApplicationUser.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a push_application_user.

  ## Examples

      iex> update_push_application_user(push_application_user, %{field: new_value})
      {:ok, %PushApplicationUser{}}

      iex> update_push_application_user(push_application_user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_push_application_user(%PushApplicationUser{} = push_application_user, attrs) do
    push_application_user
    |> PushApplicationUser.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a PushApplicationUser.

  ## Examples

      iex> delete_push_application_user(push_application_user)
      {:ok, %PushApplicationUser{}}

      iex> delete_push_application_user(push_application_user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_push_application_user(%PushApplicationUser{} = push_application_user) do
    Repo.delete(push_application_user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking push_application_user changes.

  ## Examples

      iex> change_push_application_user(push_application_user)
      %Ecto.Changeset{source: %PushApplicationUser{}}

  """
  def change_push_application_user(%PushApplicationUser{} = push_application_user) do
    PushApplicationUser.changeset(push_application_user, %{})
  end
end
