defmodule Push.PushApplicationsUsers.PushApplicationUser do
  use Ecto.Schema
  import Ecto.Changeset
  alias Push.PushApplicationsUsers.PushApplicationUser

  schema "push_applications_users" do
    field :user_id, :id, null: false
    field :push_application_id, :id, null: false

    timestamps()
  end

  @doc false
  def changeset(%PushApplicationUser{} = push_application_user, attrs) do
    push_application_user
    |> cast(attrs, [:user_id, :push_application_id])
    |> validate_required([:user_id, :push_application_id])
  end
end
