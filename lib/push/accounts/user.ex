defmodule Push.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias Push.Accounts.User

  import Ecto.Query
  alias Push.Repo

  schema "users" do
    field :email, :string
    field :full_name, :string

    field :password_hash, :string
    # recoverable
    field :reset_password_token, :string
    field :reset_password_sent_at, :utc_datetime
    # trackable
    field :sign_in_count, :integer, default: 0
    field :current_sign_in_at, :utc_datetime
    field :last_sign_in_at, :utc_datetime
    field :current_sign_in_ip, :string
    field :last_sign_in_ip, :string
    # unlockable_with_token
    field :unlock_token, :string
    # confirmable
    field :confirmation_token, :string
    field :confirmed_at, :utc_datetime
    field :confirmation_sent_at, :utc_datetime


    many_to_many :push_applications, Push.PushApplications.PushApplication, join_through: "push_applications_users"

    timestamps()
  end

  def applications_for_user(%User{} = user) do
    [user] = Repo.all(from(u in User, where: u.id == ^user.id, preload: :push_applications))
    user.push_applications #=> [%PushApplication{...}, ...]
  end

  @doc false
  def changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [:email, :full_name, :confirmation_token, :confirmed_at])
    |> validate_required([:email, :full_name])
    |> unique_constraint(:email)
  end
end
