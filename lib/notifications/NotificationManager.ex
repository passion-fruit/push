  defmodule Notifications.NotificationManager do
  @moduledoc """
  This module looks after the connections to APNS and Google Play.
  It stores the process address for each connection.
  You can think of it as a supervisor, however, we don't know what connections will be
  open. These connections are dynamic. They are open when called upon and left open
  until told otherwise. For now...
  """
  use GenServer

  require Logger

  # I'm using @name here because I want to treat this as a kind of singleton.
  # All notification requests will go through here.
  # This will allow the web part to send notification requests via this server where
  # this server will send them on to the appropriate connections.
  @name __MODULE__

  # Client

  def start_link do
    GenServer.start_link(__MODULE__, :ok, [name: @name])
  end

  @doc """
  This function allows the client to send a notification request to the server.
  To see what the request Map looks like, see: PushNotificationController.
  """
  def request(request) do
    Logger.info("request function called.")
    GenServer.cast(@name, {:add, request})
  end

  # Server

  def init(:ok) do
    {:ok, %{}}
  end

  def handle_cast({:add, request}, state) do
    import Pigeon.APNS.Notification

    Logger.info("Handling request message.")
    Logger.info(inspect(state))

    app_key = "app_#{request.app_id}_plat_#{request.platform_id}_env_#{request.env}"

    if Map.has_key?(state, app_key) do
      Logger.info("Connection to notification service exists.")
      # If the connection already exists: lookup it up.
      connection_pid = Map.get(state, app_key)

      if Process.alive?(connection_pid) do
        # Spawn another process to handle sending the push.
        Logger.info("Connection to notification service still alive.")
        create_notify_processes_and_notify(connection_pid, request)

        {:noreply, state}
      else
        # TODO: Remove the connection_pid from the map.
        # Start a new connection and create new push notifier process.
        Logger.info("Connection to notification service not alive.")
        new_connection_pid = start_conn_and_notify(request)
        Logger.info("Storing connection to notification service in state.")
        new_state = Map.put(state, app_key, new_connection_pid)

        {:noreply, new_state}
      end
    else
      Logger.info("Connection to notification service does not exist.")
      new_connection_pid = start_conn_and_notify(request)
      new_state = Map.put(state, app_key, new_connection_pid)

      {:noreply, new_state}
    end
  end

  defp start_conn_and_notify(request) do
    Logger.info("Starting connection to notification service.")
    {:ok, connection_pid} = Pigeon.APNS.start_connection(cert: request.ios_cert,
                                                         key: request.ios_key,
                                                         mode: :dev)
    # Spawn another process to handle sending the push.
    create_notify_processes_and_notify(connection_pid, request)
    # return connection_pid so we can store it in the state.
    connection_pid
  end

  defp create_notify_processes_and_notify(connection_pid, request) do
    # Logger.info("Creating Notifications.NotificationHandler processes to handle the notifications.")
    create_notify_process_and_notify(request.device_tokens, connection_pid, request)
  end

  defp create_notify_process_and_notify([device_token | tail], connection_pid, request) do
    Logger.info("Creating Notifications.NotificationHandler process to handle the notification.")
    {:ok, handler_pid} = Notifications.NotificationHandler.start_link
    Logger.info("Sending message to Notifications.NotificationHandler with the notification payload.")
    Notifications.NotificationHandler.notify(handler_pid, connection_pid, device_token, request)

    create_notify_process_and_notify(tail, connection_pid, request)
  end
  defp create_notify_process_and_notify([], _connection_pid, _request) do
    Logger.info("Sent all notifications.")
  end
end
