defmodule Notifications.NotificationHandler do
  use GenServer

  require Logger

  # Client

  def start_link do
    GenServer.start_link(__MODULE__, :ok, [])
  end

  @doc """
  This function allows the client to send a notification request to the server.
  The request should look like this:
  %{
    app_id: 1,
    platform_id: 2,
    payload: %{},
    env: 'dev' | 'prod'
    device_tokems: ["123123-123123-2313"]
  }
  """
  def notify(pid, connection_pid, device_token, request) do
    GenServer.cast(pid, {:notify, connection_pid, device_token, request})
  end

  # Server

  def init(:ok) do
    {:ok, %{}}
  end

  def handle_cast({:notify, connection_pid, device_token, request}, state) do
    Logger.info("Handling notify message.")
    import Pigeon.APNS.Notification

    # Create the notification.
    # For the topic param see: https://developer.apple.com/library/content/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/CommunicatingwithAPNs.html#//apple_ref/doc/uid/TP40008194-CH11-SW1
    # The Pigeon lib says it's optional so I've decided to use it this way because it complains if I don't include it.
    notif = Pigeon.APNS.Notification.new(
      request.msg,
      device_token,
      request.bundle_id)

    Logger.info("Pigeion sending push message / calling function.")
    # Send push notification.
    Pigeon.APNS.push(notif, to: connection_pid, on_response: handler)

    {:noreply, state}
  end

  defp handler do
    fn(%Pigeon.APNS.Notification{response: response}) ->
      Logger.info("Response from notification service")
      Logger.info(inspect(response))
      case response do
        :success ->
          Logger.info "Push successful - killing handler process."
          Process.exit(self(), :normal)
        :bad_device_token ->
          Logger.info "Bad device token!"
        :bad_collapse_id ->
          # The collapse identifier exceeds the maximum allowed size
          Logger.info "Bad collapse ID"
        :bad_device_token ->
          # The specified device token was bad. Verify that the request contains a valid token and that the token matches the environment.
          Logger.info "Bad device token"
        :bad_expiration_date ->
          # The apns-expiration value is bad.
          Logger.info "Bad collapse ID"
        :bad_message_id ->
          #	The apns-id value is bad.
          Logger.info "Bad message ID"
        :bad_priority ->
          # The apns-priority value is bad.
          Logger.info "Bad priority"
        :bad_topic ->
          # The apns-topic was invalid.
          Logger.info "Bad topic"
        :device_token_not_for_topic ->
          # The device token does not match the specified topic.
          Logger.info "Device token not for topic"
        :duplicate_headers ->
          # One or more headers were repeated.
          Logger.info "Duplicate headers"
        :idle_timeout ->
          # Idle time out.
          Logger.info "Idle timeout"
        :missing_device_token ->
          # The device token is not specified in the request :path. Verify that the :path header contains the device token.
          Logger.info "Missing device token"
        :missing_topic ->
          # The apns-topic header of the request was not specified and was required. The apns-topic header is mandatory when the client is connected using a certificate that supports multiple topics.
          Logger.info "Missing topic"
        :payload_empty ->
          # The message payload was empty.
          Logger.info "payload empty"
        :topic_disallowed ->
          # Pushing to this topic is not allowed.
          Logger.info "Topic disallowed"
        :bad_certificate ->
          #	The certificate was bad.
          Logger.info "Bad certificate"
        :bad_certificate_environment ->
          # The client certificate was for the wrong environment.
          Logger.info "Bad certificate environment"
        :expired_provider_token ->
          #	The provider token is stale and a new token should be generated.
          Logger.info "Expired provider token"
        :forbidden ->
          #	The specified action is not allowed.
          Logger.info "Forbidden"
        :invalid_provider_token ->
          #	The provider token is not valid or the token signature could not be verified.
          Logger.info "Invalid provider token"
        :missing_provider_token ->
          # No provider certificate was used to connect to APNs and Authorization header was missing or no provider token was specified.
          Logger.info "Missing provider token"
        :bad_path ->
          # The request contained a bad :path value.
          Logger.info "Bad path"
        :method_not_allowed ->
          #	The specified :method was not POST.
          Logger.info "Method not allowed"
        :unregistered ->
          #	The device token is inactive for the specified topic.
          Logger.info "Unregistered"
        :payload_too_large ->
          # The message payload was too large. The maximum payload size is 4096 bytes.
          Logger.info "Payload too large"
        :too_many_provider_token_updates ->
          # The provider token is being updated too often.
          Logger.info "Too many provider token updates"
        :too_many_requests ->
          #	Too many requests were made consecutively to the same device token.
          Logger.info "Too many requests"
        :internal_server_error ->
          #	An internal server error occurred.
          Logger.info "Internal server error"
        :service_unavailable ->
          # The service is unavailable.
          Logger.info "Service unavailable"
        :shutdown ->
          # Shutdown
          Logger.info "Shutdown"
        _error ->
          Logger.info "Some other error happened."
      end
    end
  end
end
